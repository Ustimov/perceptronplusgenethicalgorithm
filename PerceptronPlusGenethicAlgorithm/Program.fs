﻿open System
open System.IO
open System.Drawing
open System.Text

type GAPerceptron(numberOfInputs, populationSize) = 
    let chromosomeLength = 4
    let weights: float[] = Array.zeroCreate numberOfInputs
    let random = Random(0)
    let mutable population: Option<ResizeArray<string>> = None

    let activate(sum: float) = 
        if sum > 0.0 then 1.0 else -1.0

    let generatePopulation() =
        let p = ResizeArray<string>()
        let stringBuilder = StringBuilder()
        for _ in 1..populationSize do
            for _ in 1..numberOfInputs do
                for _ in 1..chromosomeLength do
                    stringBuilder.Append(random.Next(0, 2).ToString())
                    |> ignore
            p.Add(stringBuilder.ToString())
            |> ignore
            stringBuilder.Clear()
            |> ignore
        population <- Some(p)

    let crossingOver() =
        let p = ResizeArray<string>()
        for i in population.Value do
            p.Add(i)
        for i in 0..2..population.Value.Count - 1 do
            let newChromosome1 = StringBuilder()
            let newChromosome2 = StringBuilder()
            for j in 0..numberOfInputs - 1 do
                if j % 2 = 0 then
                    newChromosome1.Append(population.Value.[i].Substring(j * chromosomeLength, chromosomeLength))
                    |>ignore
                    newChromosome2.Append(population.Value.[i + 1].Substring(j * chromosomeLength, chromosomeLength))
                    |>ignore
                else
                    newChromosome2.Append(population.Value.[i].Substring(j * chromosomeLength, chromosomeLength))
                    |>ignore
                    newChromosome1.Append(population.Value.[i + 1].Substring(j * chromosomeLength, chromosomeLength))
                    |>ignore
            p.Add(newChromosome1.ToString())
            p.Add(newChromosome2.ToString())
        population <- Some(p)

    let mutate() =
        let chromosomeNumber = random.Next(0, population.Value.Count)
        let valueNumber = random.Next(0, numberOfInputs * chromosomeLength)
        let oldChromosome = population.Value.[chromosomeNumber]
        let newValue = if oldChromosome.[valueNumber] = '0' then "1" else "0"
        let newChromosome = oldChromosome.Substring(0, valueNumber) + newValue + oldChromosome.Substring(valueNumber + 1)
        population.Value.[chromosomeNumber] <- newChromosome

    let step() = 
        if population.IsNone then
            generatePopulation()
        else
            crossingOver()
            mutate()

    let setWeights(chromosome: string) =
        for i in 0..numberOfInputs - 1 do
            let binary = chromosome.Substring(i * chromosomeLength + 1, chromosomeLength - 1)
            let decimal = Convert.ToInt32(binary, 2)
            weights.[i] <- (float)decimal / 10.0
            if chromosome.[i * chromosomeLength + 1] = '1' then
                weights.[i] <- -weights.[i]

    member this.Train(trainData: float[][], desired: float[], maxError: float) = 
        let mutable error = 1.0
        while error > maxError do
            step()
            let rank = ResizeArray<string * float>()
            for chromosome in population.Value do
                setWeights(chromosome)
                let mutable errorCount = 0
                for i in 0..trainData.Length - 1 do
                    let result = this.ComputeOutput(trainData.[i])
                    if result <> desired.[i] then
                        errorCount <- errorCount + 1
                let e = (float)errorCount / (float)trainData.Length
                rank.Add(chromosome, e)
            let sorted = 
                rank
                |> Seq.sortBy (fun r -> snd r)
                |> Seq.take populationSize
                |> Seq.toList
            setWeights(fst sorted.[0])
            error <- snd sorted.[0]
            population.Value.Clear()
            for r in rank do
                population.Value.Add(fst r)
        
    member this.ComputeOutput(inputs: float[]) = 
        let mutable sum = 0.0
        for i in 0..weights.Length - 1 do
            sum <- sum + (inputs.[i] * weights.[i])
        activate(sum)

let loadInputFrom path =
    let files = Directory.GetFiles(path)
    let input = ResizeArray<float[]>();
    let digit = ResizeArray<int>()
    let black = Color.FromArgb(255, 0, 0, 0)
    for file in files do
        use bitmap = new Bitmap(file)
        let array: float[] = Array.zeroCreate (bitmap.Height * bitmap.Width)
        for i in 0..bitmap.Height - 1 do
            for j in 0..bitmap.Width - 1 do
                array.[j + i * bitmap.Width] <-
                    if bitmap.GetPixel(j, i) = black then 1.0 else 0.0
        input.Add(array)
        digit.Add(int(Path.GetFileName(file).Substring(0, 1)))
    input, digit

let train (perceptron: GAPerceptron) input digit maxError number = 
    let trainData =
        input
        |> Seq.toArray
    let desired =
        digit
        |> Seq.map (fun d -> if d = number then 1.0 else -1.0)
        |> Seq.toArray
    perceptron.Train(trainData, desired, maxError)
    |> ignore

[<EntryPoint>]
let main argv = 
    let input, digit = loadInputFrom "../../../Images/train"

    let maxError = 0.00
    let number = 7

    let perceptron = GAPerceptron(35, 1000)

    train perceptron input digit maxError number

    let input, _ = loadInputFrom "../../../Images/test7"

    printf "%d: " number
    let results =
        input
        |> Seq.map (fun i -> perceptron.ComputeOutput(i))
        |> Seq.toArray
    let recognizedCount =
        results
        |> Seq.fold (fun c r -> if r = 1.0 then c + 1 else c) 0
    printfn "[%d of %d] %A" recognizedCount results.Length results

    0
